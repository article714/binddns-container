ARG DEBIAN_CONTAINER_VERSION
ARG BASE_DEBIAN_VERSION
FROM article714/debian-based-container:${BASE_DEBIAN_VERSION}-${DEBIAN_CONTAINER_VERSION}
LABEL maintainer="C. Guychard<christophe@article714.org>"


# Container tooling

COPY container /container

# container building

RUN /container/build.sh

# Expose DNS ports
EXPOSE 53

# bind9 data & conf to external volume
VOLUME /etc/bind
